import React from 'react';
import Image from './Image';

const PlaceImg = ({ place, index = 0, className = null }) => {
  if (!place.photos?.length) {
    return 'Loading...';
  }

  if (!className) {
    className = 'object-cover';
  }
  return (
    <>
      {place.photos.length > 0 && (
        <Image className={className} src={place.photos[index]} alt="" />
      )}
    </>
  );
};

export default PlaceImg;
