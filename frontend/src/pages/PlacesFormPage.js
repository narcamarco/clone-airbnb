import React, { useEffect, useState } from 'react';
import axios from 'axios';

import Perks from '../Perks';
import PhotosUploader from '../PhotosUploader';
import AccountNav from '../AccountNav';
import { Navigate, useParams } from 'react-router-dom';

const InputHeader = ({ text }) => {
  return <h2 className="text-2xl mt-4">{text}</h2>;
};

const InputDescription = ({ text }) => {
  return <p className="text-gray-500 text-sm">{text}</p>;
};

const PreInput = ({ header, description }) => {
  return (
    <>
      <InputHeader text={header} />
      <InputDescription text={description} />
    </>
  );
};

const PlacesFormPage = () => {
  const { id } = useParams();
  const [title, setTitle] = useState('');
  const [address, setAddress] = useState('');

  const [addedPhotos, setAddedPhotos] = useState([]);
  const [description, setDescription] = useState('');
  const [perks, setPerks] = useState([]);
  const [extraInfo, setExtraInfo] = useState('');
  const [checkIn, setCheckIn] = useState('');
  const [checkOut, setCheckOut] = useState('');
  const [maxGuest, setMaxGuest] = useState(1);
  const [price, setPrice] = useState(100);
  const [redirect, setRedirect] = useState(false);

  const savePlace = async (e) => {
    e.preventDefault();

    const placeData = {
      title,
      address,
      addedPhotos,
      description,
      perks,
      extraInfo,
      checkIn,
      checkOut,
      maxGuest,
      price,
    };
    if (id) {
      await axios.put('/places', {
        id,
        ...placeData,
      });

      setRedirect(true);
    } else {
      await axios.post('/places', {
        ...placeData,
      });

      setRedirect(true);
    }
  };

  useEffect(() => {
    if (!id) {
      return;
    }

    axios.get(`/places/${id}`).then((response) => {
      const { data } = response;
      setTitle(data.title);
      setAddress(data.address);
      setAddedPhotos(data.photos);
      setDescription(data.description);
      setPerks(data.perks);
      setExtraInfo(data.extraInfo);
      setCheckIn(data.checkIn);
      setCheckOut(data.checkOut);
      setMaxGuest(data.maxGuest);
      setPrice(data.price);
    });
  }, [id]);

  if (redirect) {
    return <Navigate to={'/account/places'} />;
  }
  return (
    <div className="">
      <AccountNav />
      <form onSubmit={savePlace}>
        <PreInput
          header="Title"
          description="title for your place. should be short and catchy as in
              advertisement"
        />
        <input
          type="text"
          placeholder="title, for example: My Lovely Apartment"
          value={title}
          onChange={(ev) => setTitle(ev.target.value)}
        />

        <PreInput header="Address" description="Address to this place" />
        <input
          type="text"
          placeholder="address"
          value={address}
          onChange={(ev) => setAddress(ev.target.value)}
        />

        <PreInput header="Photos" description="more = better" />

        <PhotosUploader addedPhotos={addedPhotos} onChange={setAddedPhotos} />

        <PreInput header="Description" description="description of the place" />
        <textarea
          value={description}
          onChange={(ev) => setDescription(ev.target.value)}
        />

        <PreInput
          header="Perks"
          description="select all the perks of the places"
        />

        <div className="grid mt-2 gap-2 grid-cols-2 md:grid-cols-3 lg:grid-cols-6">
          <Perks selected={perks} onChange={setPerks} />
        </div>

        <PreInput header="Extra info" description="house rules, etc" />
        <textarea
          value={extraInfo}
          onChange={(ev) => setExtraInfo(ev.target.value)}
        />

        <PreInput
          header="Check in&out times, max guests"
          description="Add check In and out times, remember to have some window for
              cleaning the room"
        />

        <div className="grid gap-2 grid-cols-2 md:grid-cols-4">
          <div>
            <h3 className="mt-2 -mb-1">Check In time</h3>
            <input
              type="text"
              placeholder="14"
              value={checkIn}
              onChange={(ev) => setCheckIn(ev.target.value)}
            />
          </div>
          <div>
            <h3 className="mt-2 -mb-1">Check Out Time</h3>
            <input
              type="text"
              placeholder="11"
              value={checkOut}
              onChange={(ev) => setCheckOut(ev.target.value)}
            />
          </div>
          <div>
            <h3 className="mt-2 -mb-1">Max Number of Guest</h3>
            <input
              type="number"
              value={maxGuest}
              onChange={(ev) => setMaxGuest(ev.target.value)}
            />
          </div>
          <div>
            <h3 className="mt-2 -mb-1">Price per night</h3>
            <input
              type="number"
              value={price}
              onChange={(ev) => setPrice(ev.target.value)}
            />
          </div>
        </div>
        <button className="primary my-4">Save</button>
      </form>
    </div>
  );
};

export default PlacesFormPage;
