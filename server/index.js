require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const imageDownloader = require('image-downloader');
const { S3Client, PutObjectCommand } = require('@aws-sdk/client-s3');
const multer = require('multer');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');
const mime = require('mime-types');

const User = require('./models/User');
const Place = require('./models/Place');
const Booking = require('./models/Booking');
const connect = require('./db/connect');

const app = express();

const bcryptSalt = bcrypt.genSaltSync(10);
const jwtSecret = process.env.JWT_SECRET;
const bucket = process.env.BUCKET_NAME;

const getUserDataFromToken = (req) => {
  return new Promise((resolve, reject) => {
    jwt.verify(req.cookies.token, jwtSecret, {}, async (err, userData) => {
      if (err) throw err;

      resolve(userData);
    });
  });
};

app.use(express.static(path.join(__dirname, '..', 'frontend', 'build')));

app.use(express.json());
app.use(cookieParser());

app.use(helmet());
app.use(helmet.crossOriginResourcePolicy({ policy: 'cross-origin' }));
app.use(
  helmet.contentSecurityPolicy({
    useDefaults: true,
    directives: {
      'img-src': ["'self'", 'https: data:'],
    },
  })
);

app.use(
  cors({
    credentials: true,
    origin: 'https://airbnb-booking.onrender.com',
  })
);

async function uploadToS3(path, originalFilename, mimetype) {
  const client = new S3Client({
    region: 'ap-southeast-1',
    credentials: {
      accessKeyId: process.env.S3_ACCESS_KEY,
      secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
    },
  });

  const parts = originalFilename.split('.');
  const ext = parts[parts.length - 1];

  const newFilename = Date.now() + '.' + ext;

  try {
    const data = await client.send(
      new PutObjectCommand({
        Bucket: bucket,
        Body: fs.readFileSync(path),
        Key: newFilename,
        ContentType: mimetype,
        ACL: 'public-read',
      })
    );
    return `https://${bucket}.s3.amazonaws.com/${newFilename}`;
  } catch (error) {
    console.log(error);
  }
}

app.get('/api/test', (req, res) => {
  res.json('test ok');
});

app.post('/api/register', async (req, res) => {
  connect();
  const { name, email, password } = req.body;

  try {
    const userDoc = await User.create({
      name,
      email,
      password: bcrypt.hashSync(password, bcryptSalt),
    });
    res.json(userDoc);
  } catch (error) {
    res.status(422).json(error);
  }
});

app.post('/api/login', async (req, res) => {
  connect();
  const { email, password } = req.body;

  try {
    const userDoc = await User.findOne({ email });
    if (userDoc) {
      const passOk = bcrypt.compareSync(password, userDoc.password);
      const oneDay = 1000 * 60 * 60 * 24;

      if (passOk) {
        jwt.sign(
          { email: userDoc.email, id: userDoc._id },
          jwtSecret,
          {
            expiresIn: process.env.JWT_LIFETIME,
          },
          (err, token) => {
            if (err) {
              throw err;
            }

            res
              .cookie('token', token, {
                httpOnly: true,
                expires: new Date(Date.now() + oneDay),
                secure: process.env.NODE_ENV === 'production',
              })
              .json(userDoc);
          }
        );
      } else {
        res.status(422).json('pass not ok');
      }
    } else {
      res.json('not found');
    }
  } catch (error) {
    res.status(422).json(error);
  }
});

app.get('/api/profile', (req, res) => {
  connect();
  const { token } = req.cookies;

  if (token) {
    jwt.verify(token, jwtSecret, {}, async (err, userData) => {
      if (err) {
        throw err;
      }

      const { name, email, _id } = await User.findById(userData.id);

      res.json({ name, email, _id });
    });
  } else {
    res.json(null);
  }
});

app.post('/api/logout', (req, res) => {
  res.cookie('token', '', {
    httpOnly: true,
    expires: new Date(Date.now() + 1000),
  });

  // res.cookie('token', '').json(true);

  res.status(200).json({ msg: 'user logged out!' });
});

app.post('/api/upload-by-link', async (req, res) => {
  const { link } = req.body;
  const newName = 'photo' + Date.now() + '.jpg';
  await imageDownloader.image({
    url: link,
    dest: '/tmp/' + newName,
  });

  const url = await uploadToS3(
    '/tmp/' + newName,
    newName,
    mime.lookup('/tmp/' + newName)
  );
  res.json(url);
});

const photosMiddleware = multer({ dest: '/tmp' });

app.post(
  '/api/upload',
  photosMiddleware.array('photos', 100),
  async (req, res) => {
    const uploadedFiles = [];
    for (let i = 0; i < req.files.length; i++) {
      const { path, originalname, mimetype } = req.files[i];
      const url = await uploadToS3(path, originalname, mimetype);
      uploadedFiles.push(url);
    }

    res.json(uploadedFiles);
  }
);

app.post('/api/places', (req, res) => {
  connect();
  const { token } = req.cookies;
  const {
    title,
    address,
    addedPhotos,
    description,
    perks,
    extraInfo,
    checkIn,
    checkOut,
    maxGuest,
    price,
  } = req.body;

  jwt.verify(token, jwtSecret, {}, async (err, userData) => {
    if (err) {
      throw err;
    }

    const placeDoc = await Place.create({
      owner: userData.id,
      title,
      address,
      photos: addedPhotos,
      description,
      perks,
      extraInfo,
      checkIn,
      checkOut,
      maxGuest,
      price,
    });

    res.json(placeDoc);
  });
});

app.get('/api/user-places', (req, res) => {
  connect();
  const { token } = req.cookies;
  jwt.verify(token, jwtSecret, {}, async (err, userData) => {
    const { id } = userData;

    res.json(await Place.find({ owner: id }));
  });
});

app.get('/api/places/:id', async (req, res) => {
  connect();
  const { id } = req.params;

  res.json(await Place.findById(id));
});

app.put('/api/places', async (req, res) => {
  connect();
  const { token } = req.cookies;
  const {
    id,
    title,
    address,
    addedPhotos,
    description,
    perks,
    extraInfo,
    checkIn,
    checkOut,
    maxGuest,
    price,
  } = req.body;

  jwt.verify(token, jwtSecret, {}, async (err, userData) => {
    const placeDoc = await Place.findById(id);

    if (userData.id === placeDoc.owner.toString()) {
      placeDoc.set({
        title,
        address,
        photos: addedPhotos,
        description,
        perks,
        extraInfo,
        checkIn,
        checkOut,
        maxGuest,
        price,
      });

      await placeDoc.save();
      res.json('ok');
    }
  });
});

app.get('/api/places', async (req, res) => {
  connect();
  res.json(await Place.find());
});

app.post('/api/bookings', async (req, res) => {
  connect();
  const userData = await getUserDataFromToken(req);

  const { place, checkIn, checkOut, numberOfGuests, name, phone, price } =
    req.body;

  const bookingDoc = await Booking.create({
    place,
    checkIn,
    checkOut,
    numberOfGuests,
    name,
    phone,
    user: userData.id,
    price,
  });

  res.status(201).json(bookingDoc);
});

app.get('/api/bookings', async (req, res) => {
  connect();
  const userData = await getUserDataFromToken(req);

  res.json(await Booking.find({ user: userData.id }).populate('place'));
});

app.use((req, res, next) => {
  res.sendFile(
    path.resolve(__dirname, '..', 'frontend', 'build', 'index.html')
  );
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is connection on port: ${PORT}...`);
});
