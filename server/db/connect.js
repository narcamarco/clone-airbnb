const mongoose = require('mongoose');

module.exports = () => {
  mongoose
    .connect(process.env.MONGO_URL)
    .then(() => {
      console.log(`connecting to the database`);
    })
    .catch((error) => {
      console.log(error);
    });
};
